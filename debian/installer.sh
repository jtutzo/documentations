#!/usr/bin/env bash

#########################################################
# installation ssh and sftp on debian 9
#########################################################

set -o nounset
set -o errexit

if [[ "$EUID" -ne 0 ]];
then
    echo "User must be root"
    exit 1
fi

##### Start #####

iam=`who am i | awk -F" " '{print $1}'`
iam=${iam:-user}

echo "Start default configuration system..."

# Update system
apt-get update >> /dev/null

#Install vim
echo -n "Installation of vim..."
apt-get install -y vim >> /dev/null
echo "Done"

#Install git and svn
echo -n "Installation of git and svn..."
apt-get install -y git subversion >> /dev/null
echo "Done"

# Install firewald
echo -n "Installation of firewalld..."
apt-get install -y firewalld >> /dev/null
echo "Done"

# Install ssh
echo -n "Installation of ssh..."
apt-get install -y openssh-server openssh-client >> /dev/null
echo "Done"

#Install bashtools
echo -n "Installation of bashtools..."
git clone https://gitlab.com/jtbashtools/bashtools /home/${iam}/bashtools >> /dev/null
cd /home/${iam}/bashtools && ./install >> /dev/null && cd -
echo "Done"

echo -n "Create and config sftp user..."

# Create sftp user
useradd -m sftp >> /dev/null

# Change user's right
chown root:sftp /home/sftp && chmod 755 /home/sftp

echo "Done"

echo -n "Config ssh..."

# Create partage folde
mkdir -p /home/sftp/partage && chown sftp:sftp /home/sftp/partage

# Change parametre PermitRootLogin
sed -i 's|\(#\)\(PermitRootLogin.\)\(.*\)|\2no|g' /etc/ssh/sshd_config

# Config ssh and sftp
echo "
Match User sftp
    ChrootDirectory %h
    ForceCommand internal-sftp
    AllowTcpForwarding no
    GatewayPorts no
    X11Forwarding no

AllowUsers ${iam} sftp" >> /etc/ssh/sshd_config

# Add ssh service to firewalld and restart this
firewall-cmd --permanent --add-service=ssh >> /dev/null 2>&1 && /etc/init.d/firewalld restart >> /dev/null

echo "Done"

echo -n "Copy Config file..." 

# Copy git config file
cp src/gitconfig /home/${iam}/.gitconfig

# Copy bash config file
cp src/bashrc /home/${iam}/.bashrc

# Copy vim config file
cp src/vimrc /home/${iam}/.vimrc

echo "Done"

echo "Installation with success"
echo "Please restart your system and change password of sftp user"

exit 0

##### END #####

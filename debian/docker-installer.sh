#!/usr/bin/env bash

#########################################################
# installation of docker and docker-compose for debian 9
#########################################################

set -o nounset
set -o errexit

if [[ "$EUID" -ne 0 ]];
then
	echo "User must be root."
	exit 1
fi

iam=`who am i |awk -F" " '{print $1}'`
iam=${iam:-user}

##### Remove docker and docker-compose #####

echo -n "Remove docker and docker-compose..."

apt-get remove -y docker \
	docker-engine \
    docker.io >> /dev/null 2>&1 | true
	
rm -rfv /usr/local/bin/docker-compose >> /dev/null 2>&1 | true

echo "Done"

##### Install docker and dependances #####

echo -n "Install docker..."

apt-get install -y \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg2 \
    software-properties-common >> /dev/null
 
curl -fsSL https://download.docker.com/linux/$(. /etc/os-release; echo "$ID")/gpg | sudo apt-key add - >> /dev/null

add-apt-repository \
       "deb [arch=amd64] https://download.docker.com/linux/$(. /etc/os-release; echo "$ID") \
       $(lsb_release -cs) \
       stable" >> /dev/null

apt-get update >> /dev/null \
    && apt-get install -y docker-ce >> /dev/null

usermod -aG docker ${iam} >> /dev/null

echo "Done"

##### Install docker-compose #####

echo -n "Install docker-compose..."

curl -L https://github.com/docker/compose/releases/download/1.16.1/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose >> /dev/null

chmod +x /usr/local/bin/docker-compose

echo "Done"

service docker start

echo "Installation with success"
echo "Please restart machine"

exit 0

##### End #####

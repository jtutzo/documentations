# Quick start env

## Debian

Ce dossier est composé de deux scripts :
 - installer.sh
 - docker-installer.sh

installer.sh permet d'installer et de configuer les éléments de bases du système tel que le ssh et le sftp.
Il permet aussi d'installer les fichiers de configuration de bash, vim et git afin d'offrir plus de fonctionnalités.<br />
Les packets suivant seront installés : `vim`, `git`, `subversion`, `firewalld`, `openssh-server`, `openssh-client` et [`bashtools`](https://gitlab.com/jtbashtools/bashtools).<br />
Ce script est à executer en tant que root (sudo).

docker-installer.sh permet d'installer et configurer docker et docker compose.<br />
Les packets suivant seront installés : `apt-transport-https`, `ca-certificates`, `curl`, `gnup2g`, `software-properties-common`, `docker-ce` et `docker-compose`.<br />
Ce script est à executer en tant que root (sudo).

### Pré-requis

Passez en super-utilisateur
```bash
su
```

Installez le packet `sudo`
```bash
apt-get install sudo
```

Ajoutez l'utilisateur courant au groupe `sudo`
```bash
adduser [user] sudo
```

Redémarrez le système
```bash
reboot
```

### Installation et configuration du système de base

Executez le script `installer.sh` et redémarrez le système
```bash
cd debian/ && sudo ./installer.sh && sudo reboot
```

### Installation de docker et docker-compose

Executez le script `docker-installer.sh` et redémarrez le système
```bash
cd debian/ sudo && ./docker-installer.sh && sudo reboot
```

### Exécution des tests

cf [Exécution et configuration des tests](./debian/test/README.md)

## Centos

Ce dossier est composé de deux scripts :
 - installer.sh
 - docker-installer.sh

installer.sh permet d'installer et de configuer les éléments de bases du système tel que le ssh et le sftp.
Il permet aussi d'installer les fichiers de configuration de bash, vim et git afin d'offrir plus de fonctionnalités.<br />
Les packets suivant seront installés : `vim`, `git`, `subversion`, `firewalld`, `openssh-server`, `openssh-client` et [`bashtools`](https://gitlab.com/jtbashtools/bashtools).<br />
Ce script est à executer en tant que root (sudo).

docker-installer.sh permet d'installer et configurer docker et docker compose.<br />
Les packets suivant seront installés : `yum-utils`, `device-mapper-persistent-data`, `lvm2`, `docker-ce` et `docker-compose`.<br />
Ce script est à executer en tant que root (sudo).

### Pré-requis

Passez en super-utilisateur
```bash
su
```

Configurez votre carte réseau
```bash
sed -i 's|\(^ONBOOT=\)\(.*\)|\1yes|g' /etc/sysconfig/network-scripts/[carte reseau]
```

Redemmarez le `network`
```bash
service network restart
```

Ajoutez l'utilisateur courant au groupe `wheel`
```bash
usermod -aG wheel [user]
```

Redémarrez le système
```bash
reboot
```

### Installation et configuration du système de base

Executez le script `installer.sh` et redémarrez le système
```bash
cd centos/ && sudo ./installer.sh && sudo reboot
```

### Installation de docker et docker-compose

Executez le script `docker-installer.sh` et redémarrez le système
```bash
cd centos/ && sudo ./docker-installer.sh && reboot
```

### Exécution des tests

cf [Exécution et configuration des tests](./centos/test/README.md)
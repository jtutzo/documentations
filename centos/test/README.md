# Configuration et exécution des tests
> Les tests sont effectué dans des container docker afin d'éviter au maximum des erreurs sur le système.<br />
Veuillez installer `docker-compose` pour les réaliser.

## Configuration de l'environement de test

Créez l'environement de test
```
cd centos/test/ && docker-compose up -d
```

Accedez à l'environement de test
```bash
docker exec -ti quick-start-debian-test bash
```

Dans le conteneur, redéfinez le mot de passe de l'utilsateur `user`
```bash
passwrd user
```

## Exécution des tests

Dans le container, connectez vous à l'utilisateur `user`
```bash
su user
```

Dans le container, testez le script `installer.sh`
```bash
cd quick-start/ && sudo ./installer.sh
```

Dans le container, testez le script `docker-installer.sh`
```bash
cd quick-start/ && sudo ./docker-installer.sh
```